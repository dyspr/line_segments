var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var segments = []
var numOfSegments = 52
var sw = 0.025
var frame = 0
var speed = 0.01
var blink = 0

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < numOfSegments; i++) {
    var p1, p2
    p1 = new Point(random(-0.4, 0.4), random(-0.4, 0.4))
    p2 = new Point(random(-0.4, 0.4), random(-0.4, 0.4))
    segments.push(new Segment(p1, p2))
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < segments.length; i++) {
    if (blink < 2 + 2 * sin(frame * Math.PI)) {
      stroke(255 * (i / segments.length))
    } else {
      stroke(255 - 255 * (i / segments.length))
    }
    strokeCap(ROUND)
    strokeWeight(boardSize * sw)
    segments[i].display()
  }

  frame += speed * deltaTime
  blink += speed * deltaTime
  if (blink >= 4) {
    blink = 0
  }
  if (frame >= 1) {
    frame = 0
    var p1, p2
    p1 = new Point(random(-0.4, 0.4), random(-0.4, 0.4))
    p2 = new Point(random(-0.4, 0.4), random(-0.4, 0.4))
    segments = segments.splice(1)
    segments.push(new Segment(p1, p2))
  }

}

class Point {
  constructor(x, y) {
    this.x = x
    this.y = y
  }
}

class Segment {
  constructor(start, end) {
    this.start = new Point(start.x, start.y)
    this.end = new Point(end.x, end.y)
  }

  display() {
    line(windowWidth * 0.5 + boardSize * this.start.x, windowHeight * 0.5 + boardSize * this.start.y, windowWidth * 0.5 + boardSize * this.end.x, windowHeight * 0.5 + boardSize * this.end.y)
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
